module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  persistRepoData: true,
  logLevel: 'debug',
  logFile: 'renovate.log.json',
  baseBranches: 'master',
  autodiscover: false,
  onboardingConfig: {
    extends: [
      'config:base',
      ':preserveSemverRanges',
      ':rebaseStalePrs',
      ':enableVulnerabilityAlertsWithLabel(\'security\')',
      'group:recommended',
    ],
  },
  repositories: ['dsandrocicero/nodejs-teste'],
};
