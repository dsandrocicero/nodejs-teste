import http from 'k6/http';
import { sleep } from 'k6';
export const options = {
    vus: 1000,
    duration: '30s',
    thresholds: {
        "http_req_duration": ["p(95)<10000"],
        "http_req_failed": ["rate<0.01"],
      }
};
export default function () {
  http.get('http://localhost:3000');
  sleep(1);
}

